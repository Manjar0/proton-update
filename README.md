Simple script to quickly fetch the latest Proton-GE (GloriousEggrol) releases.
It will download and extract the Proton-GE releases in the steam compatibility tools directory.
Restart steam after fetching new releases to use the new proton.

To use the update script, set the CTDIR variable to the desired location and it should fetch the Proton-GE releases and extract them in the set location.

To fetch a specific release, you can provide the corresponding tag as an argument like so:
`./protonupdate.sh 7-5`
