#!/bin/bash

# This script fetches the latest Glorious Eggroll Proton release
# Or takes an argument and fetches that specific tag instead

# Runtime variables:
# Proton directory
CTDIR="${HOME}/.steam/root/compatibilitytools.d/"

# Glorious eggrol releases uri
EGGROLLURI="https://github.com/GloriousEggroll/proton-ge-custom/releases"
GHAPI="https://api.github.com/repos/GloriousEggroll/proton-ge-custom/releases"
#https://github.com/GloriousEggroll/proton-ge-custom/releases/download/GE-Proton7-35/GE-Proton7-35.tar.gz

# Temporary files location
MKTMP=$(mktemp -d)


fetch_tags() {
    # Fetch latest tag
    if [[ -n "${1}" ]]; then
        unset LT_TAR LT_URI
        ARG_TAG='true'
    else
        LT_TAR=$(curl -s ${GHAPI} \
            | grep -Eo '"GE-Proton.*\.tar\.gz"'\
            | sort -rV | uniq | tr -d '"' | head -n 1)
        LT_TAG="${LT_TAR%%\.tar.gz}"
        LT_URI="${EGGROLLURI}/download/${LT_TAG}/${LT_TAR}"
    fi
    # Fetch argument tag, format = int-int
    if [[ -n "${1}" ]]; then
        ARG_TAG="GE-Proton${1}"
        ARG_TAR="GE-Proton${1}.tar.gz"
        ARG_URI="${EGGROLLURI}/download/${ARG_TAG}/${ARG_TAR}"
        if [[ $(curl -o /dev/null --silent -Iw '%{http_code}' "${ARG_URI}") == '404' ]]; then
            echo "Proton release: ${ARG_URI} not found, exiting..."
            echo "Hint: only give version number as argument, example: ${0} 7-50"
            exit 1
        fi
    fi
}


download_release() {
    # Check fetched release
    if [[ -z ${LT_TAR} && -z ${ARG_TAR} ]];  then
        echo "Failed to find any releases.. Exiting..."
        exit 1
    fi
    # Download tarballs
    if [[ -d ${MKTMP} ]]; then
        cd "${MKTMP}"
        if [[ -n "${LT_TAR}" && ! $(find "${CTDIR}" -maxdepth 1 -name "${LT_TAG}") ]]; then
            curl -L "${LT_URI}" -o "${LT_TAR}"
        else
            if [[ -z "${LT_TAR}" && -z "${ARG_TAG}" ]]; then
                echo "No latest tags found at ${EGGROLLURI}"
            elif [[ -n "${ARG_TAG}" ]]; then
                echo "${ARG_URI}"
            else
                echo "Proton version with latest tag: ${LT_TAG} found in ${CTDIR}, nothing to do.."
            fi
        fi
        if [[ -n "${ARG_TAR}" && ! $(find "${CTDIR}" -maxdepth 1 -name "${ARG_TAG}") ]]; then
            curl -L "${ARG_URI}" -o "${ARG_TAR}"
        else
            if [[ -z "${ARG_TAG}" ]]; then
                return
            fi
            if [[ -z "${ARG_TAR}" ]]; then
                echo "No ${ARG_TAG} tags found at ${EGGROLLURI}"
            else
                echo "Proton version with tag: ${ARG_TAG} found in ${CTDIR}, nothing to do.."
            fi
        fi
    else
        echo "Destination: ${MKTMP} not found, exiting..."
        exit 1
    fi
}


install_proton() {
    # Copy tarballs to proton dir
    mkdir -p "${CTDIR}"
    if [[ -d "${CTDIR}" ]]; then
        cd "${CTDIR}"
        if [[ -f "${MKTMP}/${LT_TAR}" ]]; then
            tar -xf "${MKTMP}/${LT_TAR}"
            echo "Downloaded Proton version with tag: ${LT_TAR}"
        fi
        if [[ -f "${MKTMP}/${ARG_TAR}" ]]; then
            tar -xf "${MKTMP}/${ARG_TAR}"
            echo "Downloaded Proton version with tag: ${ARG_TAR}"
        fi
    else
        echo "${CTDIR} not found... Set CTDIR to a valid proton directory"
    fi
    # Delete temp files
    if [[ -d "${MKTMP}" ]]; then
        rm "${MKTMP}" -rf
    fi
}


# Main functions are executed here
exec_main() {
    fetch_tags "${1}"
    download_release
    install_proton
    exit 0
}

exec_main "${1}"